ActionController::Routing::Routes.draw do |map|
  map.route_tags_keys << :tag << :title
  map.root :controller => 'home', :action => 'index', :tag => 'root', :title => 'Hello World'
  map.connect '/hello', :controller => 'home', :action => 'hello', :tag => 'hello', :title => 'Hi!'
  map.connect '/world', :controller => 'home', :action => 'world', :tag => 'world', :title => 'Now you know!'
end
