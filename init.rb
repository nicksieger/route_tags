require 'route_tags'

require 'action_controller/routing'
class ActionController::Routing::Route
  include RoutingExtensions::RouteTags
end
require 'action_controller/resources'
class ActionController::Routing::RouteSet::Mapper
  include RoutingExtensions::MapperSupport
end
