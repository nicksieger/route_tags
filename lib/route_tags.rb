# http://weblog.jamisbuck.org/2006/10/26/monkey-patching-rails-extending-routes-2
# was very helpful in developing this extension.
module RoutingExtensions
  # This extension allows you to attach extra fixed data to each route.
  # You do this by specifying, e.g.,
  #   :tag => "public", :activity => "read"
  # along with the rest of your route parameters.
  #
  # In the case above, the resulting options will be passed along
  # with each request that matches that route. Consider this a lightweight
  # way of "tagging" routes.
  #
  # Resource-based routing works a little differently than normal
  # map.connect and named routes in the way it handles options.
  # Unfortunately this affects nested resources:
  #
  #     map.with_options :area => "people" do |pmap|
  #     pmap.resources :people do |people|
  #       people.resources :projects, :area => "people"
  #     end
  #
  # In the example above, people.resources will not see the :area
  # option, so it needs to be repeated.
  #
  module RouteTags
    def self.included(base)
      base.alias_method_chain :recognition_extraction, :tags
      base.alias_method_chain :requirements, :tags
      base.alias_method_chain :extra_keys, :tags
    end

    delegate :route_tags_keys, :to => "ActionController::Routing::RouteSet::Mapper"

    def requirements_with_tags
      Hash[*(requirements_without_tags.select{|k,v| !route_tags_keys.include?(k)}.flatten)]
    end

    def extra_keys_with_tags(*args)
      extra_keys_without_tags(*args) - route_tags_keys
    end

    def recognition_extraction_with_tags
      extractions = recognition_extraction_without_tags
      route_tags_keys.each do |k|
        if requirements_without_tags[k]
          extractions << "params[#{k.inspect}] = requirements_without_tags[#{k.inspect}].dup"
        else
          extractions << "params[#{k.inspect}] = nil unless params[#{k.inspect}]"
        end
      end
      extractions
    end
  end

  module MapperSupport
    def self.included(base)
      base.alias_method_chain :action_options_for, :tags
      base.instance_eval do
        @@route_tags_keys = []
        cattr_accessor :route_tags_keys
      end
    end

    def action_options_for_with_tags(action, resource, method = nil)
      options = action_options_for_without_tags(action, resource, method)
      route_tags_keys.each do |k|
        options[k] = resource.options[k] if resource.options[k]
      end
      options
    end
  end
end

